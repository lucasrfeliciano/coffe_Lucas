//
//  ViewController.swift
//  coffe_Lucas
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Coffe:Decodable {
    let image : String
}

class ViewController: UIViewController, UITableViewDataSource {

    var listaDeCafe:[Coffe] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeCafe.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Minha Celula", for: indexPath) as! MyCell
        let cafe = self.listaDeCafe[indexPath.row]
        
        cell.img_cafe.image = UIImage(named: cafe.image)
        cell.img_cafe.kf.setImage(with: URL(string: cafe.image))
        return cell
    }
    
 
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        self.tableview.dataSource = self
        super.viewDidLoad()
        getAtor()
        
    }
    
    func getAtor() {
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of: [Coffe].self){
            response in
            if let cafe = response.value{
                self.listaDeCafe = cafe
                print(self.listaDeCafe)
            }
            self.tableview.reloadData()
        }

    }
}
